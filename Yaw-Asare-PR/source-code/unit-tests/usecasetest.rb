require_relative "../VIN"
require 'test/unit'

class TestUseCases < Test::Unit::TestCase

    def setup  
        @vin = VIN.new
    end

    def testAddSubscriber
        response = @vin.addSubscriber("john doe", "j@email.com", "8152341242", 
                          "Wilson St.", "Chicago", "Illinois",60616)
        
        assert(@vin.errors.empty?, "should have no errors")
        assert_equal(@vin.numUsers, 1)
        assert_equal(@vin.subscribers.length, 1)
        assert_equal(response['id'], 1)

        @vin.addSubscriber("", "k@email.com", "7213843223",
                         "Wilson St.", "Chicago", "Illinois", 60232)
    
        assert(!@vin.errors.empty?)
        assert_equal(@vin.subscribers.length, 1)
    end

    def testUpdateSubscriber
        response = @vin.addSubscriber("john doe", "j@email.com", "8152341242", 
                          "Wilson St.", "Chicago", "Illinois",60616)
        
        assert(@vin.errors.empty?, "should have no errors")
        assert_equal(@vin.numUsers, 1)
        assert_equal(@vin.subscribers.length, 1)
        assert_equal(response['id'], 1)

        @vin.updateSubscriber(1, "new name", "j@email.com", "8152341242", 
                          "Wilson St.", "Chicago", "Illinois", 60616)
        assert(@vin.errors.empty?, "should have no errors")
        assert_equal(@vin.numUsers, 1)
        assert_equal(@vin.subscribers.length, 1)
        assert_equal(@vin.subscribers[0].name, "new name")

        @vin.updateSubscriber(3, "new name", "j@email.com", "8152341242", 
                          "Wilson St.", "Chicago", "Illinois", 60616)
        assert(!@vin.errors.empty?)
    end

    def testGetSubscriber
        response = @vin.addSubscriber("john doe", "j@email.com", "8152341242", 
                          "Wilson St.", "Chicago", "Illinois",60616)
        
        assert(@vin.errors.empty?, "should have no errors")
        assert_equal(@vin.numUsers, 1)
        assert_equal(@vin.subscribers.length, 1)
        assert_equal(response['id'], 1)
    
        response = @vin.getSubscriber(1)
        assert_equal(response['name'], "john doe")
        assert_equal(response['email'], "j@email.com")
    end

    def testAddAdmin
        response = @vin.addAdmin("Admin doe")

        assert(@vin.errors.empty?)
        assert_equal(@vin.numUsers, 1)
        assert_equal(@vin.admins.length, 1)
        assert_equal(response['id'], 1)

        response = @vin.addAdmin("")

        assert(!@vin.errors.empty?)
        assert_equal(response['id'], nil)
    end

    def testUpdateAdmin
        @vin.addAdmin("Admin Smith")
        @vin.addAdmin("Admin Doe")
       
        response = @vin.updateAdmin(1, "Admin Smith Jr.")
        assert(response['errors'].empty?)
        assert(@vin.admins[0].name, "Admin Smith Jr.")

        response = @vin.updateAdmin(1, "")
        assert(!response['errors'].empty?)
        assert(@vin.admins[0].name, "Admin Smith Jr.")
    end

    def testGetAdmin
        @vin.addAdmin("Admin Smith")
        @vin.addAdmin("Admin Doe")
        @vin.addAdmin("Admin Wood")

        response = @vin.getAdmin(2)
        assert_equal(2, response['id'])
        assert_equal("Admin Doe", response['name'])

        response = @vin.getAdmin(3)
        assert_equal(3, response['id'])
        assert_equal("Admin Wood", response['name'])

        response = @vin.getAdmin(20)
        assert(!response['errors'].empty?)
    end

    def testGetAdmins
        @vin.addAdmin("Admin Smith")
        @vin.addAdmin("Admin Doe")
        @vin.addAdmin("Admin Wood")

        response = @vin.getAdmins
        assert_equal(response['admins'].length, 3)
        admins = response['admins']
        assert_equal(admins[1][:name], "Admin Doe")
    end
end

